FROM centos:7 as base
WORKDIR /app
COPY deployment.sh .
RUN useradd -ms /bin/bash ec2-user && \
    useradd -ms /bin/bash jpu && \
    useradd -ms /bin/bash centos && \
    yum install -y wget && \
    yum install -y dos2unix && \
    dos2unix deployment.sh && \
    chmod +x deployment.sh && \
    yum remove dos2unix -y && \
    curl -sL https://rpm.nodesource.com/setup_6.x | bash - && \
    yum install nodejs -y && \
    npm i -g babel-cli
ENTRYPOINT ["/bin/bash"]


FROM centos:7 as base
WORKDIR /app
COPY deployment.sh .
RUN useradd -ms /bin/bash ec2-user && \
    useradd -ms /bin/bash jpu && \
    useradd -ms /bin/bash centos && \
    yum install -y wget && \
    yum install -y dos2unix && \
    dos2unix deployment.sh && \
    chmod +x deployment.sh && \
    yum remove dos2unix -y && \
    yum install -y java-1.8.0-openjdk
ENTRYPOINT ["/bin/bash"]


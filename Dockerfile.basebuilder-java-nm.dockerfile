FROM jpu-base-builder:java
WORKDIR /app
COPY nm-deployment.sh .
RUN yum install -y python-setuptools wireshark dos2unix && \
    easy_install supervisor && \
    dos2unix nm-deployment.sh && \
    chmod +x nm-deployment.sh && \
    mkdir /var/log/supervisor && \
    mkdir -p /etc/supervisor/conf.d
ENTRYPOINT ["/bin/bash"]
